#ifndef CATITEMEDIT_H
#define CATITEMEDIT_H

#include <QFrame>
#include <QtSql>

#include "dialogtpl.h"
#include "ui_catitemframe.h"

class QSqlQuery;

namespace STORE {
namespace Catalogue {
namespace Item{

class Data;

class List : public QList<Data*> {

    public:
        List() : QList<Data*>() {}
        Data *findPointer( int Id ) const;
};

class Data : public QObject {

    Q_OBJECT

    public:
        Data( QObject *parent = 0) :
            QObject( parent ), is_local(true), pParentItem(0) {}
        Data( QObject *parent, QSqlQuery &qry);

        // QImage QPixMap Пиктограмма
        QVariant Id;
        QString Code;
        QString Title;
        QDateTime From;
        QDateTime To;
        bool is_local;
        QString Comment;
        Data *pParentItem; //Родительский подраздел
        //int ID; //ID
        bool Deleted;
        List Children;

    public:
        bool isActive() const;
        bool isNew() const;
        bool isSameAs( Data *D ) const;
};

class Frame : public QFrame {

    Q_OBJECT

    public:
        Frame( QWidget *parent = 0 );
        virtual ~Frame();

    private:
        Ui::CatItemFrame ui;
        Data *Block ;

    public:
        void setDataBlock( Data *D) { Block = D; load(); }


    public slots:
        void is_good( bool *pOK );
        void load();
        bool save();

    signals: void error_message( const QString &);

};

class Dialog : public CommonDialog {

    Q_OBJECT

    private:
        Frame *pFrame;

    public:
        Dialog( QWidget *parent = 0 );
        virtual ~Dialog();

        void setDataBlock( Data *D) {pFrame->setDataBlock(D) ;}

};

} //namespace Item
} // namespace Catalogue
} //namespace STORE

#endif // CATITEMEDIT_H
