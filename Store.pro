#-------------------------------------------------
#
# Project created by QtCreator 2017-12-07T19:31:19
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Store
TEMPLATE = app


SOURCES += main.cpp     \
        application.cpp \
        catalogue.cpp   \
        catitemedit.cpp \
        dialogtpl.cpp   \
        mainwindow.cpp \
        posaction.cpp


HEADERS  += \
        application.h   \
        catalogue.h     \
        catitemedit.h   \
        dialogtpl.h     \
        mainwindow.h \
        posaction.h


FORMS += \
        buttonsframe.ui \
        catitemframe.ui
