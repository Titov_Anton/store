#include "catitemedit.h"

#include <QtSql>

namespace STORE {
namespace Catalogue {
namespace Item{

Data *List::findPointer(int Id) const {
    Data *D;
    foreach( D, *this) {
        bool OK;
        int cId = D->Id.toInt(&OK);
        if ( OK && cId == Id ) return D;
        Data *R = D->Children.findPointer( Id );
        if ( R ) return R;
    }

    return 0;
}

Data::Data( QObject *parent, QSqlQuery &qry)
    : QObject(parent) {

    Id          = qry.value( "iid" );
    Code        = qry.value( "code" ).toString();
    Title       = qry.value( "title" ).toString();
    From        = qry.value( "valid_from" ).toDateTime();
    To          = qry.value( "valid_to" ).toDateTime();
    is_local    = qry.value( "islocal" ).toBool();
    Comment     = qry.value( "acomment" ).toString();
    pParentItem = 0;
    Deleted = false;

//  Data *pParentItem;
//    "  rid_parent ,        \n"
//    "  alevel ,            \n"

}

bool Data::isActive() const {

    if ( From.isValid() ) {
        if( From > QDateTime::currentDateTime() ) return false;
    }
    if ( To.isValid() ) {
        if ( To < QDateTime::currentDateTime() ) return false;
    }
    return true;
}

 bool Data::isNew() const {
     if (!Id.isValid() ) return true;
     if (Id.isNull() ) return true;
     return false;
 }

 bool Data::isSameAs( Data *D ) const {
    if ( isNew() ) {
        if( !D->isNew() ) return false;
          return property("temp_id") == D->property("temp_id");

    } else {
        if ( !D->isNew() ) return false;
        return D->Id == Id;
    }
}

Frame::Frame( QWidget *parent )
    : QFrame( parent ) {

    ui.setupUi( this );
    Block = 0;
}

Frame::~Frame() {

}

/**TODO: Тут сделать нормальную проверку */
void Frame::is_good( bool *pOK ){
    *pOK = true;
}


void Frame::load() {
    if ( !Block ) return;
    ui.edtCode->setText( Block->Code );
    ui.edtTitle->setText( Block->Title );
    ui.cbxLocal->setChecked( Block->is_local );
    ui.edtComment->setText( Block->Comment );
    ui.cbxFromEnable->setChecked (Block->From.isValid() );
    ui.edtFrom->setDateTime( Block->From );
    ui.cbxToEnable->setChecked( Block->To.isValid() );
    ui.edtTo->setDateTime( Block->To );

}

bool Frame::save() {

    if ( !Block ) return false;

    Block->Code     = ui.edtCode->text().simplified();
    Block->Title    = ui.edtTitle->text().simplified();
    Block->is_local = ui.cbxLocal->isChecked();
    Block->Comment  = ui.edtComment->toPlainText().trimmed();
    if ( ui.cbxFromEnable->isChecked() ) {
        Block->From = ui.edtFrom->dateTime();
    } else {
        Block->From = QDateTime();
    }

    if ( ui.cbxToEnable->isChecked() ) {
        Block->To = ui.edtFrom->dateTime();
    } else {
        Block->To = QDateTime();
    }

    return true;

}



Dialog::Dialog( QWidget *parent)
    : CommonDialog( parent ) {

    {
        Frame *F = pFrame = new Frame ( this );
        setCentralFrame( F );
    }
}

Dialog::~Dialog() {

}

} //namespace Item
} //namespace Catalogue
} //namespace STORE
