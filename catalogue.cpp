#include <QAction>
#include <QMenu>

#include <QtSql>

#include "catalogue.h"
#include "posaction.h"

#define DATA_PTR(D,I,VALUE)                                 \
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
   if ( !D ) return VALUE

#define DATE_STR(DATE) (    \
    DATE.isValid() ? DATE.toString( "dd.MM.yyyy" ) : QString() \
    )

namespace STORE {
namespace Catalogue {

Model::Model( QObject *parent)
    : QAbstractItemModel ( parent ), LastTempId(1) {

    QSqlQuery qry ;
    qry.prepare(
         "select                \n"
         "  iid ,               \n"
         "  code ,              \n"
         "  title ,             \n"
         "  valid_from ,        \n"
         "  valid_to  ,         \n"
         "  islocal,            \n"
         "  acomment ,          \n"
         "  rid_parent ,        \n"
         "  alevel              \n"
         "  from catalogue      \n"
         "  order by alevel ;   \n"
    );

    qry.setForwardOnly(true);
    if ( qry.exec() ) {
    while (qry.next() ) {
        bool OK = false;
        QVariant PI = qry.value( "rid_parent" );
        int ParentId = PI.toInt( &OK );

        Item::Data *P = 0;
        if ( OK && !PI.isNull() )
            P = Cat.findPointer( ParentId );

        if ( P ){
            Item::Data *D = new Item::Data ( P, qry );

            P->Children.append( D );
            D->pParentItem = P;

        } else {
            Item::Data *D = new Item::Data ( this, qry );
            Cat.append( D );
            D->pParentItem = 0;
         }

    }
    } else {
        qCritical() << "Cannot execute query";
        QSqlError Err = qry.lastError();
        qCritical() << Err.databaseText();
        qCritical() << Err.driverText();
        qCritical() << Err.nativeErrorCode();
    }
//    //TODO это тестовый каталог. Заменить на настоящий
//    {
//    Item::Data *D = new Item::Data( this );
//    D->Code     = "111";
//    D->Title    = "Phisics";
//    D->From     = QDateTime::currentDateTime();
//    D->To       = QDateTime();
//    D->is_local = false;
//    Cat.append(D);
//    }    {
//    Item::Data *D = new Item::Data( this );
//    D->Code     = "222";
//    D->Title    = "Maths";
//    D->From     = QDateTime::currentDateTime();
//    D->To       = QDateTime();
//    D->is_local = false;
//    Cat.append(D);
//    }    {
//    Item::Data *D = new Item::Data( this );
//    D->Code     = "333";
//    D->Title    = "Biology";
//    D->From     = QDateTime::currentDateTime();
//    D->To       = QDateTime();
//    D->is_local = false;
//    Cat.append(D);
//    }    {
//    Item::Data *D = new Item::Data( this );
//    D->Code     = "444";
//    D->Title    = "Geography";
//    D->From     = QDateTime::currentDateTime();
//    D->To       = QDateTime();
//    D->is_local = true;
//    D->Comment  = "blahblahblah";
//    Cat.append(D);
//    }    {
//    Item::Data *D = new Item::Data( this );
//    D->Code     = "555";
//    D->Title    = "Politics";
//    D->From     = QDateTime::currentDateTime();
//    D->To       = QDateTime();
//    D->is_local = false;
//    Cat.append(D);
//    }
}

Model::~Model () {
}

int Model::rowCount(const QModelIndex &parent) const {

    if (! parent.isValid() ) {
        return Cat.size(); //TODO Заменить на правильное количество
    } else {
//        DATA_PTR( P, parent, 0 );
        Item::Data *P = (Item::Data*)parent.internalPointer()  ;  \
        if ( !P ) return 0;
        return P->Children.size();
    }
}

int Model::columnCount(const QModelIndex &parent) const {

        return 6; //Code, Title, from, to, is_local, Comment
}

QVariant Model::data(const QModelIndex &I, int role) const {

    //if ( role != Qt::DisplayRole ) return QVariant();

    switch ( role ) {
    case Qt::DisplayRole:  return dataDisplay(I);
    case Qt::TextAlignmentRole: return dataTextAlignment(I);
    case Qt::ForegroundRole : return dataForeground(I);
    case Qt::FontRole : return dataFont(I);
    case Qt::ToolTipRole : return dataToolTip(I);
    case Qt::UserRole+1 : {
//        DATA_PTR( D, I, false);
        Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
        if ( !D ) return false;
        return D->Deleted;
    }
    default: return QVariant();
    }

    //return QString( "%1,%2" ).arg( I.row()).arg( I.column() );
}

QVariant Model::headerData(int section,
                    Qt::Orientation orientation, int role) const {
    if ( orientation != Qt::Horizontal ) return QAbstractItemModel::headerData(section, orientation, role);

    switch( role ){
    case Qt::DisplayRole : switch ( section ){
        case 0: return tr( "Code" );
        case 1: return tr( "Title");
        case 2: return tr( "From" );
        case 3: return tr( "To" );
        case 4: return tr( "Local" );
        default: return QVariant();
        }
    case Qt::TextAlignmentRole: return QVariant(Qt::AlignBaseline | Qt::AlignHCenter);
    case Qt::ForegroundRole:
    {
        //TODO Сделать шрифт жирным
        QFont F;
        F.setBold( true );
        return F;
    }

    default: return QVariant();
    }
}

//Item::Data *Model::dataDataPointer( const QModelIndex &I) const {

//    int R = I.row();
//    if (R < 0 || R >= Cat.size() ) return 0;
//    return Cat[R];
//}

QVariant Model::dataFont( const QModelIndex &I ) const {
//    DATA_PTR( D, I, QVariant() );
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
    if ( !D ) return QVariant();
    QFont F;
    if ( D->Deleted ) F.setStrikeOut( true );
    return F;
}

QVariant Model::dataForeground( const QModelIndex &I ) const {
//    DATA_PTR( D, I, QVariant() );
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
    if ( !D ) return QVariant();
    QColor Result = D->is_local ? QColor( "blue" ) : QColor( "black" );
    if ( !D->isActive() ) Result.setAlphaF( 0.25 );
    return Result;
}

QVariant Model::dataToolTip( const QModelIndex &I ) const {
//    DATA_PTR( D, I, QVariant() );
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
    if ( !D ) return QVariant();
    switch ( I.column() ) {
    case 2: {
        if ( !D->To.isValid() ) return QVariant();
        return tr( "Valid to: %1" ).arg( D->To.toString("dd.MM.yyyy") );
    }
    default: return QVariant();
    }
}

QVariant Model::dataDisplay ( const QModelIndex &I) const {

//    DATA_PTR( D, I, QVariant()) ;
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
    if ( !D ) return QVariant();
    switch ( I.column() ) {
    case 0: return D->Code;
    case 1: return D->Title;
    case 2: return DATE_STR( D->From );
    case 3: return DATE_STR( D->To );
    case 4: return D->is_local ? tr( "LOCAL" ) : QString();
    case 5: return D->Comment.isEmpty() ? QString() : tr("CMT");
    default: return QVariant();
    }
}

QVariant Model::dataTextAlignment( const QModelIndex &I ) const {

    int result = Qt::AlignVCenter;
    result |= I.column() == 1 ? Qt::AlignLeft : Qt::AlignHCenter;
    return result;
}

QModelIndex Model::index( int row, int column, const QModelIndex &parent ) const {
    if ( parent.isValid() ) {
        Item::Data *D = (Item::Data*) (parent.internalPointer())  ;  \
        if ( !D ) return QModelIndex();
//        DATA_PTR( D,  parent, QModelIndex() );
        return createIndex( row, column, (void*)D );
    } else {
        if (row < 0 || row >= Cat.size() ) return QModelIndex();
        return createIndex( row, column, (void*)( Cat[row]) ) ;
    }
}

QModelIndex Model::parent( const QModelIndex &I ) const {
    DATA_PTR( D, I, QModelIndex() );
    Item::Data *P = D->pParentItem;
    if ( !P ) return QModelIndex();

    int Row = -1;
    Item::Data *GP = P->pParentItem;
    if (GP) {
         for (int i = 0; i<GP->Children.size(); i++) {
             if ( GP->Children[i]->isSameAs(P) ) {
                 Row = i;
                 break;
             }
         }
     } else {
         for (int i = 0; i<GP->Children.size(); i++) {
             if ( Cat[i]->isSameAs(P) ) {
                 Row = i;
                 break;
             }
         }
     }
        return createIndex( Row, 0, P);
        if ( Row<0 ) {
            qWarning() << "Cannot find item";
            return QModelIndex();
        }


    return QModelIndex();
}

void Model::editItem( const QModelIndex &I, QWidget *parent){

//    DATA_PTR(D, I, );
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \
    if ( !D ) return;
    Item::Dialog Dia( parent );

    Dia.setDataBlock( D );
    beginResetModel();
    Dia.exec();
    endResetModel();
}

void Model::newItem( const QModelIndex &parentI, QWidget *parent ) {
    if ( parentI.isValid() ){
        //TODO Сделать добавление нового элемента не обязательно в корень каталога
        qWarning() << "Cannot add non-top level item";
        return;
    }
    Item::Data *D = new Item::Data ( this );
    if (!D) {
        qWarning() << "Cannot create new item";
        return;
    }
    Item::Dialog Dia( parent );
    Dia.setDataBlock( D );
    if (Dia.exec() == QDialog::Accepted ) {
        beginResetModel();
        Cat.append( D );
        D->setProperty( "temp_id", tempId() );
        endResetModel();
    } else {
        delete D;
    }
}

void Model::delItem( const QModelIndex &I, QWidget *parent ) {
    //TODO спросить у пользователя, уверен ли он, что хочет удалить элемент
//    DATA_PTR( D, I, );
    Item::Data *D = (Item::Data*) (I.internalPointer())  ;  \

    if ( !D ) return;
    //TODO Исходим из того, что модель линейна
    beginResetModel();
    if ( D->isNew() ) {
        Item::Data *P = D->pParentItem;
        if (P) {
            P->Children.removeAt( I.row() );
        } else {
            Cat.removeAt( I.row() );
        }

        Cat.removeAt( I.row() );
        delete D;
    } else {
        D->Deleted = !D->Deleted;
    }
    endResetModel();

}

TableView::TableView( QWidget *parent )
    : QTableView( parent ) {

    setContextMenuPolicy( Qt::CustomContextMenu );
    connect( this, SIGNAL(customContextMenuRequested(QPoint)),
             this, SLOT(contextMenuRequested(QPoint)) );



    Model *M = new Model( this );
    setModel( M );



    {
        PosAction *A = actEditItem = new PosAction( this );
        A->setText( tr("Edit") );
        connect( A, SIGNAL( editItem(QModelIndex, QWidget*) ),
                 M, SLOT( editItem(QModelIndex, QWidget *) ) );
        addAction(A);
    }{
        PosAction *A = actNewItem = new PosAction( this );
        A->setText( tr( "Add" ) );
        connect( A, SIGNAL( editItem(QModelIndex,QWidget*) ),
                 M, SLOT( newItem(QModelIndex,QWidget*) ) );
    }{
        PosAction *A = actDelItem = new PosAction( this );
        A->setText( tr( "Delete" ) );
        connect( A, SIGNAL( editItem(QModelIndex,QWidget*) ),
                 M, SLOT( delItem(QModelIndex,QWidget*) ) );
    }{
        QHeaderView *H = verticalHeader();
        H->setSectionResizeMode( QHeaderView::ResizeToContents );
    }{
        QHeaderView *H = horizontalHeader();
        H->setSectionResizeMode( QHeaderView::ResizeToContents );
        H->setSectionResizeMode( 1, QHeaderView::Stretch );
    }
    setColumnHidden( 3, true );
    setColumnHidden( 4, true );
}

TableView::~TableView() {
}

void TableView::contextMenuRequested( const QPoint &p) {

    QMenu M (this);

    QModelIndex I = indexAt(p);
    if ( I.isValid() ) {

        actEditItem->I = I;
        actEditItem->pWidget = this;
        M.addAction ( actEditItem );
    }
    {
        actDelItem->I = I;
        actDelItem->pWidget = this;
        if (I.data( Qt::UserRole+1).toBool() ) {
            actDelItem->setText( tr( "Restore" ) );
        } else {
            actDelItem->setText( tr( "Delete" ) );
        }
        M.addAction ( actDelItem );
    }{
        actNewItem->I = QModelIndex();
        actNewItem->pWidget = this;
        M.addAction ( actNewItem );
    }
    M.exec( mapToGlobal(p) );

}

} //namespace Catalogue
} //namespace STORE
